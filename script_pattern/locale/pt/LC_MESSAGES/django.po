# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-15 12:52+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: script_pattern/admin.py:38
msgctxt "script_pattern"
msgid "Url patterns"
msgstr ""

#: script_pattern/apps.py:7
msgctxt "script_pattern"
msgid "Script pattern"
msgstr ""

#: script_pattern/const.py:14
msgctxt "script_pattern"
msgid "Head"
msgstr ""

#: script_pattern/const.py:15
msgctxt "script_pattern"
msgid "Body"
msgstr ""

#: script_pattern/const.py:21
msgctxt "script_pattern"
msgid "Top"
msgstr ""

#: script_pattern/const.py:22
msgctxt "script_pattern"
msgid "Bottom"
msgstr ""

#: script_pattern/models.py:16
msgctxt "script_pattern"
msgid "Pattern"
msgstr ""

#: script_pattern/models.py:19
msgctxt "script_pattern"
msgid ""
"Case-sensitive. A missing trailing slash does also match to files which "
"start with the name of the pattern, e.g., '/admin' matches '/admin.html' "
"too. Also pattern allow an asterisk (*) as a wildcard and a dollar sign ($) "
"to match the end of the URL, e.g., '/*.jpg$'."
msgstr ""

#: script_pattern/models.py:30
msgctxt "script_pattern"
msgid "Script url"
msgstr ""

#: script_pattern/models.py:33
msgctxt "script_pattern"
msgid "Script urls"
msgstr ""

#: script_pattern/models.py:47
msgctxt "script_pattern"
msgid "Name"
msgstr ""

#: script_pattern/models.py:50
msgctxt "script_pattern"
msgid "Is active"
msgstr ""

#: script_pattern/models.py:53
msgctxt "script_pattern"
msgid "Tag"
msgstr ""

#: script_pattern/models.py:58
msgctxt "script_pattern"
msgid "Position"
msgstr ""

#: script_pattern/models.py:63
msgctxt "script_pattern"
msgid "Script"
msgstr ""

#: script_pattern/models.py:68
msgctxt "script_pattern"
msgid "Allowed"
msgstr ""

#: script_pattern/models.py:71
msgctxt "script_pattern"
msgid "The URLs which are allowed to include script block."
msgstr ""

#: script_pattern/models.py:77
msgctxt "script_pattern"
msgid "Disallowed"
msgstr ""

#: script_pattern/models.py:80
msgctxt "script_pattern"
msgid "The URLs which are not allowed to include script block."
msgstr ""

#: script_pattern/models.py:87
msgctxt "script_pattern"
msgid "Script block"
msgstr ""

#: script_pattern/models.py:90
msgctxt "script_pattern"
msgid "Script blocks"
msgstr ""
